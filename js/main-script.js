document.querySelector('.sign-in-button').addEventListener('click', function(e){
	// не даем браузеру кинуть на новую страницу
	e.preventDefault();
	var xhr = new XMLHttpRequest();
	//получаем значения полей вводимых юзером
	var userEmail = document.querySelector('.e-mail').value;
	var userPassword = document.querySelector('.password').value;
	//помещаем в переменную данные в формате urlencoded
	var body = 'email=' + encodeURIComponent(userEmail) +
	  '&password=' + encodeURIComponent(userPassword);
	//Подготовка к отправке данных на сервер
	xhr.open("POST", 'http://netology-hj-ajax.herokuapp.com/homework/login_json', true);
	//Указываем кодировку
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	//Отправляем данные
	xhr.send(body);
	//создаем переменные 
	var errorMessage = document.querySelector('.error-message');
	var eMail = document.querySelector('.label-email');
	var password = document.querySelector('.label-password');
	var signInButton = document.querySelector('.sign-in-button');
	var preloader = document.querySelector('.preloader');
	//убираем форму и отображаем прелоадер
	errorMessage.innerHTML = "";
	eMail.style.display = "none";
	password.style.display = "none";
	signInButton.style.display = "none";
	preloader.style.display = "block";
	// скрываем прелоадер
	xhr.addEventListener('loadend', function(){
		preloader.style.display = "none";
	});
	xhr.addEventListener('load', function(){
		if (xhr.status == 200) {
			var personal = document.querySelector('.personal-data');
			var userName = document.querySelector('.user-name');
			var avatar = document.querySelector('.profile-avatar');
			var country = document.querySelector('.user-country');
			var hobbies = document.querySelector('.user-hobbies');
			//Распарсиваем получанные данные
			var user = JSON.parse(xhr.responseText);
			//вставляем данные о юзере	
			personal.style.display = 'block';
			avatar.src = user.userpic;
			userName.innerHTML = user.name + " " + user.lastname;
			country.innerHTML = "Country: " + user.country;
			hobbies.innerHTML = "Hobbies: " + user.hobbies.join(', ');
		    // вывести результат
		    console.log(xhr.responseText ); // responseText — текст ответа.
		} else {
			//снова показываем форму + сообщение об ошибке
			eMail.style.display = "inline-block";
			password.style.display = "inline-block";
			signInButton.style.display = "inline-block";
			errorMessage.innerHTML = xhr.status + ': ' + xhr.statusText;
			errorMessage.style.color = "red";
		    // обработать ошибку
		}
	});
	// событие "клик" для sign-out кнопки
	document.querySelector('.sign-out-button').addEventListener('click', function(e){
		var personal = document.querySelector('.personal-data');
		var userName = document.querySelector('.user-name');
		var avatar = document.querySelector('.profile-avatar');
		var country = document.querySelector('.user-country');
		var hobbies = document.querySelector('.user-hobbies');
		//убираем данные о юзере и возвращаем исходную форму
		personal.style.display = 'none';
		avatar.src = '';
		userName.innerHTML = '';
		country.innerHTML = '';
		hobbies.innerHTML = '';
		eMail.style.display = "inline-block";
		password.style.display = "inline-block";
		signInButton.style.display = "inline-block";
		eMail.value = "";
		password.value = "";		
	});
});








